const uSchema = require('../entity/Userschema')

let typeormConfiguration ={
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "typeorm_db",
    "synchronize": false,
    "logging": false,
    "entities": [
       uSchema
    ]
}

module.exports = typeormConfiguration;