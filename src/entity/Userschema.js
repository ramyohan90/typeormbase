const { EntitySchema } = require('typeorm');
const UserData = require('../model/User').User;

module.exports = new EntitySchema({
    name: "Users",
    target: UserData,
    columns: {
        id:{
            primary: true,
            generated: true,
            type: "int",
        },
        name: {
            type: "varchar"
        },
        location: {
            type: "varchar"
        }
    }
})