const typeorm = require('typeorm');
const iUser = require('./model/User').User;
const ormConfig = require('./config/ormconfig');

typeorm.createConnection(ormConfig).then(async (connection)=>{
    console.log("Opening Connection!")
    const userData =new iUser(0, "sriram", "tuticorin1");
    return await connection.manager.save(userData).then((resp)=>{
        console.log("Inserted successfully!")
    })
})