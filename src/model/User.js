
class User{
    constructor(id, name, location){
        this.id = id;
        this.name = name;
        this.location = location;
    }
}

module.exports = {
    User : User
}